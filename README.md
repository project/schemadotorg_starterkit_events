Table of contents
-----------------

* Introduction
* Features
* Schema.org types

Introduction
------------

The **Schema.org Blueprints Starter Kit: Events module** provides
a Schema.org Event type with a view using the Smart Date module.


Features
--------

- Creates an /events view.
- Adds a default shortcut to view Events (/events).


Schema.org types
----------------

Below are the Schema.org types created by this starter kit.

- **Event** (node:Event)  
  Content which describes something happening at a specific time.  
  <https://schema.org/Event>


```bash
# Collect default content uuids. 
echo 'shortcut:'; ddev drush sqlq "SELECT CONCAT('- ', uuid) FROM shortcut";

# Export default content.
ddev drush default-content:export-module schemadotorg_starterkit_events;
